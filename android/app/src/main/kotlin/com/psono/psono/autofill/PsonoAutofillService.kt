package com.psono.psono.autofill

import android.os.CancellationSignal
import android.service.autofill.AutofillService
import android.service.autofill.FillCallback
import android.service.autofill.FillRequest
import android.service.autofill.SaveCallback
import android.service.autofill.SaveRequest

class PsonoAutofillService: AutofillService() {
    /**
     * Called by the Android system do decide if a screen can be autofilled by the service.
     *
     * Inspired / copied from (under Mozilla Public License 2.0) :
     * https://github.com/mozilla-lockwise/lockwise-android/blob/df21ab6442f4a6315d0e3683139825646509a0aa/app/src/main/java/mozilla/lockbox/LockboxAutofillService.kt
     * https://github.com/mozilla-lockwise/lockwise-android/blob/afbd758bb9bfbfcf4910fbebc9bec28c46865530/app/src/main/java/mozilla/lockbox/autofill/ParsedStructureBuilder.kt
     *
     */
    override fun onFillRequest(request: FillRequest, cancellationSignal: CancellationSignal,
                               callback: FillCallback) {

        val structure = request.fillContexts.last().structure
        val activityPackageName = structure.activityComponent.packageName
        if (this.packageName == activityPackageName) {
            callback.onFailure(null)
            return
        }

        val nodeNavigator = ViewNodeNavigator(structure, activityPackageName)
        val parsedStructure = ParsedStructureBuilder(nodeNavigator).build() as ParsedStructure

        if (parsedStructure.passwordId == null && parsedStructure.usernameId == null) {
            callback.onFailure(null)
            return
        }

        val fillResponse = AutofillHelper.buildFillResponse(this, parsedStructure)

        // If there are no errors, call onSuccess() and pass the response
        callback.onSuccess(fillResponse)
    }

    /**
     * Called when the user requests the service to save the contents of a screen.
     */
    override fun onSaveRequest(request: SaveRequest, callback: SaveCallback) {
        callback.onFailure("onSaveRequest not implemented")
    }

    /**
     * Called when the Android system connects to service.
     */
    override fun onConnected() {
        super.onConnected()
    }

    /**
     * Called when the Android system disconnects from the service.
     */
    override fun onDisconnected() {
        super.onDisconnected()
    }


}