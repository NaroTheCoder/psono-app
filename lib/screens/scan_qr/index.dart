import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:psono/components/_index.dart' as component;

class ScanQR extends StatefulWidget {
  static String tag = 'scan-config-screen';
  @override
  _ScanQRState createState() => _ScanQRState();
}

class _ScanQRState extends State<ScanQR> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  String? qrScan = '';
  bool redirect = false;
  bool redirectInitiated = false;
  QRViewController? controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (redirect && !redirectInitiated) {
        Navigator.of(context).pop(qrScan);
        redirectInitiated = true;
      }
    });

    return component.ScaffoldDark(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          Expanded(flex: 7, child: _buildQrView(context)),
          Expanded(
            flex: 1,
            child: Center(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextButton(
                      child: new Text(
                          FlutterI18n.translate(context, "FLIP_CAMERA")),
                      style: TextButton.styleFrom(
                        foregroundColor: Color(0xFFb1b6c1),
                      ),
                      onPressed: () {
                        controller?.flipCamera();
                      },
                    ),
                  ),
                  Expanded(
                    child: TextButton(
                      child: new Text(FlutterI18n.translate(context, "ABORT")),
                      style: TextButton.styleFrom(
                        foregroundColor: Color(0xFFb1b6c1),
                      ),
                      onPressed: () {
                        int count = 0;
                        Navigator.of(context).popUntil((_) => count++ >= 2);
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      cameraFacing: CameraFacing.front,
      onQRViewCreated: _onQRViewCreated,
      formatsAllowed: [BarcodeFormat.qrcode],
      overlay: QrScannerOverlayShape(
        borderColor: Colors.red,
        borderRadius: 10,
        borderLength: 30,
        borderWidth: 10,
        cutOutSize: scanArea,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((Barcode configString) async {
      setState(() {
        qrScan = configString.code;
        redirect = true;
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
