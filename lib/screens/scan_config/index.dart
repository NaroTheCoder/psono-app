import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_host.dart' as managerHost;
import 'package:psono/services/storage.dart';

class ScanConfigScreen extends StatefulWidget {
  static String tag = 'scan-config-screen';
  @override
  _ScanConfigScreenState createState() => _ScanConfigScreenState();
}

class _ScanConfigScreenState extends State<ScanConfigScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool redirect = false;

  void _showErrorDiaglog(String title, String? content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(FlutterI18n.translate(context, title)),
          content: Text(FlutterI18n.translate(context, content!)),
          actions: <Widget>[
            TextButton(
              child: Text(FlutterI18n.translate(context, "CLOSE")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        String? result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScanQR()),
        );

        Map? configMap;
        Config config;
        // v1
        // String result2 = """{
        //   "ConfigJson": {
        //     "backend_servers": [{
        //         "title": "Psono.pw",
        //         "domain": "example.com",
        //         "url": "https://example2.com"
        //       }
        //     ],
        //     "allow_custom_server": true,
        //     "allow_registration": true,
        //     "allow_lost_password": true,
        //     "authentication_methods": ["AUTHKEY", "LDAP"],
        //     "more_links": [{
        //         "href": "https://doc.psono.com/",
        //         "title": "DOCUMENTATION",
        //         "class": "fa-book"
        //       }, {
        //         "href": "privacy-policy.html",
        //         "title": "PRIVACY_POLICY",
        //         "class": "fa-user-secret"
        //       }, {
        //         "href": "https://www.psono.com",
        //         "title": "ABOUT_US",
        //         "class": "fa-info-circle"
        //       }
        //     ]
        //   }
        // }""";
        //
        // v2
        // String result2 = """{
        //   "version": 2,
        //   "config": {
        //     "verify_key": "7a04c60e944e5820da9ddf2e9310bec4b576242bd0a5751b0c473d4347620cab",
        //     "url": "https://example.com/server"
        //   }
        // }""";

        if (result == null) {
          return;
        }

        if (!mounted) {
          return;
        }
        component.Loader.show(context);

        try {
          configMap = jsonDecode(result);
          config = Config.fromJson(configMap as Map<String, dynamic>);
        } on FormatException {
          return;
        }

        if (config.version == 2) {
          await managerHost.approveHost(
              config.config!.url!, config.config!.verifyKey!);

          reduxStore.dispatch(
            InitiateLoginAction(
              config.config!.url!,
              "",
            ),
          );

          await storage.write(
            key: 'serverUrl',
            value: config.config!.url!,
            iOptions: secureIOSOptions,
          );
          try {
            result = await managerHost.loadRemoteConfig(config.config!.url!);
            try {
              configMap = jsonDecode(result);
              config = Config(
                  version: 1,
                  configJson:
                      ConfigJson.fromJson(configMap as Map<String, dynamic>));
            } on FormatException {
              return;
            }
          } on apiClient.ServiceUnavailableException {
            component.Loader.hide();
            if (!mounted) {
              return;
            }
            _showErrorDiaglog(
              FlutterI18n.translate(context, "SERVER_OFFLINE"),
              FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
            );
            return;
          } on HandshakeException {
            component.Loader.hide();
            if (!mounted) {
              return;
            }
            _showErrorDiaglog(
              FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
              FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
            );
            return;
          } catch (e) {
            component.Loader.hide();
            if (!mounted) {
              return;
            }
            _showErrorDiaglog(
              FlutterI18n.translate(context, "SERVER_OFFLINE"),
              FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
            );
            return;
          }
        }

        await storage.write(
          key: 'config',
          value: jsonEncode(config),
          iOptions: secureIOSOptions,
        );

        reduxStore.dispatch(
          ConfigUpdatedAction(
            config,
          ),
        );
        if (config.configJson != null &&
            config.configJson!.backendServers != null &&
            config.configJson!.backendServers!.length > 0 &&
            config.configJson!.backendServers![0].url != null) {
          reduxStore.dispatch(
            InitiateLoginAction(
              config.configJson!.backendServers![0].url,
              "",
            ),
          );

          await storage.write(
            key: 'serverUrl',
            value: config.configJson!.backendServers![0].url,
            iOptions: secureIOSOptions,
          );
        }
        component.Loader.hide();
        if (!mounted) {
          return;
        }
        Navigator.pushReplacementNamed(context, '/');
      });
    });

    return component.ScaffoldDark(
      body: component.Loading(),
    );
  }
}
