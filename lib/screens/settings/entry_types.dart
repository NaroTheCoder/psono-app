import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class SettingEntryTypes extends StatefulWidget {
  static String tag = 'settings-password-generator-screen';

  @override
  _SettingEntryTypesState createState() => _SettingEntryTypesState();
}

class _SettingEntryTypesState extends State<SettingEntryTypes> {
  bool enabled = false;
  String? message;
  datastoreModel.Datastore? datastore;
  List blueprints = [];

  void _showErrorDiaglog(String title, String? content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(FlutterI18n.translate(context, title)),
          content: Text(FlutterI18n.translate(context, content!)),
          actions: <Widget>[
            TextButton(
              child: Text(FlutterI18n.translate(context, "CLOSE")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> loadDatastore() async {
    component.Loader.show(context);
    datastoreModel.Datastore? newDatastore;
    try {
      newDatastore = await managerDatastoreSetting.getSettingsDatastore();
    } on apiClient.ServiceUnavailableException {
      if (context.mounted) {
        _showErrorDiaglog(
          FlutterI18n.translate(context, "SERVER_OFFLINE"),
          FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
        );
      }
      return;
    } on apiClient.UnauthorizedException {
      managerDatastoreUser.logout();

      if (context.mounted) {
        Navigator.pushReplacementNamed(context, '/signin/');
      }
      return;
    } finally {
      component.Loader.hide();
    }
    setState(() {
      datastore = newDatastore;
    });
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List blueprints = itemBlueprint.getBlueprints();
    blueprints.sort((a, b) => FlutterI18n.translate(context, a.name)
        .compareTo(FlutterI18n.translate(context, b.name)));

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'ENTRY_TYPES',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnSuccess(
            onPressed: () async {
              if (datastore != null) {
                // reduxStore.dispatch(
                //   PasswordGeneratorSettingAction(
                //     passwordLength.text,
                //     lettersUppercase.text,
                //     lettersLowercase.text,
                //     numbers.text,
                //     specialChars.text,
                //   ),
                // );

                await datastore!.save();
              }

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(FlutterI18n.translate(context, "SAVE_SUCCESS")),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE"),
          ),
        ),
      ),
      backgroundColor: const Color(0xFFebeeef),
      body: Card(
        child: Container(
          constraints: BoxConstraints.expand(),
          child: DataTable(
            onSelectAll: (b) {},
            sortAscending: true,
            columns: <DataColumn>[
              DataColumn(
                label: Text(FlutterI18n.translate(
                  context,
                  'ENTRY_TYPE',
                )),
              ),
              DataColumn(
                label: Text(FlutterI18n.translate(
                  context,
                  'VISIBLE',
                )),
              ),
            ],
            rows: datastore == null
                ? []
                : blueprints.map(
                    (bp) {
                      onChanged(value) async {
                        var found = false;
                        for (var i = 0; i < datastore!.dataKV.length; i++) {
                          if (datastore!.dataKV[i]['key'] != bp.settingField) {
                            continue;
                          }
                          datastore!.dataKV[i]['value'] = value;
                          found = true;
                          break;
                        }
                        if (!found) {
                          datastore!.dataKV.add(
                            {
                              'key': bp.settingField,
                              'value': value,
                            },
                          );
                        }

                        setState(() {
                          datastore = datastore;
                        });
                      }

                      var checked =
                          bp.settingFieldDefault; // fallback if not found
                      for (var i = 0; i < datastore!.dataKV.length; i++) {
                        if (datastore!.dataKV[i]['key'] != bp.settingField ||
                            datastore!.dataKV[i]['value'] == null) {
                          continue;
                        }
                        checked = datastore!.dataKV[i]['value'];
                        break;
                      }

                      return DataRow(
                        cells: [
                          DataCell(
                            Text(FlutterI18n.translate(
                              context,
                              bp.name,
                            )),
                            showEditIcon: false,
                            placeholder: false,
                          ),
                          DataCell(Switch(
                            value: checked,
                            onChanged: onChanged,
                          )),
                        ],
                      );
                    },
                  ).toList(),
          ),
        ),
      ),
    );
  }
}
