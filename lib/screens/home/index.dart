import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:psono/app_config.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/page_route_no_transition.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/pin/index.dart';
import 'package:psono/services/autofill.dart' as autofillService;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/offline_cache.dart' as offlineCache;
import 'package:psono/services/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var appConfig = AppConfig.of(context)!;
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      // clean storage on fresh install
      SharedPreferences.getInstance().then((prefs) async {
        if (prefs.getBool('psono_app_first_run') ?? true) {
          offlineCache.delete();
          await storage.deleteAll();
          prefs.setBool('psono_app_first_run', false);
        }
      });

      String? serverUrl = await storage.read(key: 'serverUrl');
      String? username = await storage.read(key: 'username');
      String? token = await storage.read(key: 'token');
      String? sessionSecretKey = await storage.read(key: 'sessionSecretKey');
      String? lastServerConnectionTimeSinceEpoch =
          await storage.read(key: 'lastServerConnectionTimeSinceEpoch');
      String? lastCacheTimeSinceEpoch =
          await storage.read(key: 'lastCacheTimeSinceEpoch');
      String? complianceMaxOfflineCacheTimeValid =
          await storage.read(key: 'complianceMaxOfflineCacheTimeValid');
      String? secretKey = await storage.read(key: 'secretKey');
      String? privateKey = await storage.read(key: 'privateKey');
      String? publicKey = await storage.read(key: 'publicKey');
      String? lockscreenPin = await storage.read(key: 'lockscreenPin');
      bool complianceDisableDeleteAccount =
          (await storage.read(key: 'complianceDisableDeleteAccount')) == 'true';
      bool complianceDisableOfflineMode =
          (await storage.read(key: 'complianceDisableOfflineMode')) == 'true';
      String? userId = await storage.read(key: 'userId');
      String? userEmail = await storage.read(key: 'userEmail');
      String? userSauce = await storage.read(key: 'userSauce');
      String? configJson = await storage.read(key: 'config');
      bool passedAutofillOnboarding =
          (await storage.read(key: 'passedAutofillOnboarding')) == 'true';

      Config? config;
      if (configJson != null) {
        try {
          Map configMap = jsonDecode(configJson);
          config = Config.fromJson(configMap as Map<String, dynamic>);
        } on FormatException {
          // pass
        }
      }

      if (serverUrl == null || serverUrl == '') {
        if (config != null &&
            config.configJson != null &&
            config.configJson!.backendServers != null &&
            config.configJson!.backendServers!.length > 0 &&
            config.configJson!.backendServers![0].url != null) {
          serverUrl = config.configJson!.backendServers![0].url;
        } else {
          serverUrl = appConfig.defaultServerUrl;
        }
      }

      Uint8List? sessionSecretKeyBin;
      if (sessionSecretKey != null) {
        sessionSecretKeyBin = converter.fromHex(sessionSecretKey);
      }

      int lastServerConnectionTimeSinceEpochInt = 0;
      if (lastServerConnectionTimeSinceEpoch != null) {
        try {
          lastServerConnectionTimeSinceEpochInt =
              int.parse(lastServerConnectionTimeSinceEpoch);
        } catch (e) {
          // pass
        }
      }

      int lastCacheTimeSinceEpochInt = 0;
      if (lastCacheTimeSinceEpoch != null) {
        try {
          lastCacheTimeSinceEpochInt = int.parse(lastCacheTimeSinceEpoch);
        } catch (e) {
          // pass
        }
      }

      int complianceMaxOfflineCacheTimeValidInt = 31536000;
      if (complianceMaxOfflineCacheTimeValid != null) {
        try {
          complianceMaxOfflineCacheTimeValidInt =
              int.parse(complianceMaxOfflineCacheTimeValid);
        } catch (e) {
          // pass
        }
      }

      Uint8List? secretKeyBin;
      if (secretKey != null) {
        secretKeyBin = converter.fromHex(secretKey);
      }

      Uint8List? publicKeyBin;
      if (publicKey != null) {
        publicKeyBin = converter.fromHex(publicKey);
      }

      Uint8List? privateKeyBin;
      if (privateKey != null) {
        privateKeyBin = converter.fromHex(privateKey);
      }

      reduxStore.dispatch(
        InitiateStateAction(
          serverUrl!,
          username,
          token,
          sessionSecretKeyBin,
          lastServerConnectionTimeSinceEpochInt,
          lastCacheTimeSinceEpochInt,
          secretKeyBin,
          publicKeyBin,
          privateKeyBin,
          lockscreenPin,
          complianceDisableDeleteAccount,
          complianceDisableOfflineMode,
          complianceMaxOfflineCacheTimeValidInt,
          userId,
          userEmail,
          userSauce,
          config,
        ),
      );

      bool autofillSupported = await autofillService.isSupported();

      await new Future.delayed(const Duration(milliseconds: 500));

      if (!mounted) return;

      if (autofillSupported && !passedAutofillOnboarding) {
        if (context.mounted) {
          Navigator.pushReplacementNamed(context, '/autofill_onboarding/');
        }
      } else if (lockscreenPin == null || lockscreenPin == '') {
        if (context.mounted) {
          Navigator.pushReplacementNamed(context, '/pin_configuration_screen/');
        }
      } else if (token == null || serverUrl == null || serverUrl == '') {
        if (context.mounted) {
          Navigator.pushReplacementNamed(context, '/signin/');
        }
      } else {
        if (context.mounted) {
          Navigator.of(context).pushReplacement(
            NoTransitionPageRoute(builder: (context) => PinScreen()),
          );
        }
      }
    });

    return component.ScaffoldDark(
      body: component.Loading(hideLoadingIndicator: true),
    );
  }
}
