import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/blueprint.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/services/manager_share.dart' as managerShare;

class EditSecretScreen extends StatefulWidget {
  static String tag = 'secret-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final datastoreModel.Item? item;
  final List<String?>? path;
  final List<String?>? relativePath;

  const EditSecretScreen({
    this.parent,
    this.datastore,
    this.share,
    this.item,
    this.path,
    this.relativePath,
  });

  @override
  _EditSecretScreenState createState() => _EditSecretScreenState();
}

class _EditSecretScreenState extends State<EditSecretScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _advanced = false;
  late Secret _secret;
  bool secretLoaded = false;
  Blueprint? _bp;
  datastoreModel.Item? item;
  List<TextEditingController> controllers = [];
  bool _obscurePassword = true;
  late FToast _fToast;

  final logo = Hero(
    tag: 'hero',
    child: SvgPicture.asset(
      'assets/images/logo.svg',
      semanticsLabel: 'Psono Logo',
      height: 70.0,
    ),
  );

  Future<void> loadSecret() async {
    if (this.widget.item == null ||
        this.widget.item!.secretId == null ||
        this.widget.item!.secretKey == null) {
      return;
    }
    component.Loader.show(context, const Color(0xFFFFFFFF));
    Secret secret = await managerSecret.readSecret(
        this.widget.item!.secretId!, this.widget.item!.secretKey!);
    setState(() {
      _secret = secret;
      secretLoaded = true;
    });
    component.Loader.hide();
  }

  _EditSecretScreenState({this.item});

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);

    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (this.widget.item!.secretId != null) {
        loadSecret();
        setState(() {
          _bp = itemBlueprint.getBlueprint(this.widget.item!.type);
        });
      }
    });
  }

  @override
  void dispose() {
    component.Loader.hide();
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future handleRefresh() async {
      await loadSecret();
    }

    int getFieldLength() {
      if (_bp == null) {
        return 0;
      }
      if (_advanced) {
        return _bp!.fields!.length + 1;
      } else {
        int advancedCount =
            _bp!.fields!.where((field) => field.position == 'advanced').length;
        return _bp!.fields!.length - advancedCount + 1;
      }
    }

    if (this.widget.item!.secretId == null) {
      return component.ScaffoldDark(
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              const SizedBox(height: 56.0),
              component.AlertInfo(
                text: FlutterI18n.translate(
                  context,
                  "ACCESS_DENIED",
                ),
              ),
              const SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnPrimary(
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                    text: FlutterI18n.translate(context, "BACK"),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    if (!secretLoaded) {
      return const Scaffold();
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(
              '${FlutterI18n.translate(context, "EDIT")} ${FlutterI18n.translate(context, _bp != null ? _bp!.name : 'UNKNOWN_TYPE')}'),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: const Color(0xFFebeeef),
        body: Card(
          child: RefreshIndicator(
            onRefresh: handleRefresh,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              child: Form(
                key: _formKey,
                child: ListView.builder(
                  // Let the ListView know how many items it needs to build.
                  itemCount: getFieldLength(),
                  // Provide a builder function. This is where the magic happens.
                  // Convert each item into a widget based on the type of item it is.
                  itemBuilder: (context, index) {
                    if (index == getFieldLength() - 1) {
                      // last element, render the save button

                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: component.BtnSuccess(
                          onPressed: () async {
                            if (!_formKey.currentState!.validate()) {
                              return;
                            }
                            component.Loader.show(context);

                            for (final field in _bp!.fields!.where(
                                (field) => field.inputFormatter != null)) {
                              if (_secret.data.containsKey(field.name)) {
                                field.inputFormatter!.formatEditUpdate(
                                  TextEditingValue.empty,
                                  TextEditingValue(
                                    text: _secret.data[field.name],
                                  ),
                                );
                                _secret.data[field.name] =
                                    field.inputFormatter!.getUnmaskedText();
                              }
                            }

                            _secret.save();

                            List<String?> itemPath =
                                List<String?>.from(this.widget.relativePath!);
                            itemPath.add(this.widget.item!.id);

                            if (this.widget.item!.shareId != null) {
                              shareModel.Share share =
                                  await managerShare.readShare(
                                widget.item!.shareId,
                                this.widget.item!.shareSecretKey,
                              );

                              share.item!.name = _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                share.item!.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              this.widget.item!.name =
                                  _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                this.widget.item!.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              await share.save();
                            } else if (this.widget.share == null) {
                              datastoreModel.Datastore datastore =
                                  await managerDatastorePassword
                                      .getPasswordDatastore(
                                this.widget.datastore!.datastoreId,
                              );

                              List<String> pathCopy =
                                  new List<String>.from(itemPath);
                              List search =
                                  managerDatastorePassword.findInDatastore(
                                pathCopy,
                                datastore.data,
                              );
                              datastoreModel.Item remoteItem =
                                  search[0][search[1]];

                              remoteItem.name = _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                remoteItem.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              this.widget.item!.name =
                                  _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                this.widget.item!.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              await datastore.save();
                            } else {
                              shareModel.Share share =
                                  await managerShare.readShare(
                                this.widget.share!.shareId,
                                this.widget.share!.shareSecretKey,
                              );

                              List<String> pathCopy =
                                  new List<String>.from(itemPath);
                              List search =
                                  managerDatastorePassword.findInDatastore(
                                pathCopy,
                                share.folder,
                              );
                              datastoreModel.Item remoteItem =
                                  search[0][search[1]];

                              remoteItem.name = _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                remoteItem.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              this.widget.item!.name =
                                  _secret.data[_bp!.titleField];
                              if (_bp!.urlfilterField != null) {
                                this.widget.item!.urlfilter =
                                    _secret.data[_bp!.urlfilterField];
                              }

                              await share.save();
                            }
                            component.Loader.hide();
                            return;
                          },
                          text: FlutterI18n.translate(context, "SAVE"),
                        ),
                      );
                    } else {
                      final field = _bp!.fields![index];

                      String? content = '';
                      List? contentKV = [];

                      if (_secret.data.containsKey(field.name) &&
                          _secret.data[field.name] is String) {
                        content = _secret.data[field.name];
                      } else if (_secret.data.containsKey(field.name) &&
                          _secret.data[field.name] is List) {
                        contentKV = _secret.data[field.name];
                      }

                      TextInputType? keyboardType;
                      bool obscureText = false;
                      int? maxLines = 1;
                      List<DropdownMenuItem<String>> buttons = [];
                      List<MaskTextInputFormatter> inputFormatters = [];
                      if (field.inputFormatter != null) {
                        inputFormatters.add(field.inputFormatter!);
                        if (content != null) {
                          content = field.inputFormatter!
                              .formatEditUpdate(TextEditingValue.empty,
                                  TextEditingValue(text: content))
                              .text;
                        }
                      }

                      if (field.field == 'input' && field.type == 'text') {
                      } else if (field.field == 'input' &&
                          field.type == 'password') {
                        obscureText = true && _obscurePassword;
                        buttons.add(
                          DropdownMenuItem<String>(
                            value: "show-hide",
                            child: Row(children: <Widget>[
                              Icon(
                                _obscurePassword
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: const Color(0xFF2dbb93),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0),
                                child: field.name == 'credit_card_pin'
                                    ? Text(_obscurePassword
                                        ? FlutterI18n.translate(
                                            context,
                                            "SHOW_PIN",
                                          )
                                        : FlutterI18n.translate(
                                            context,
                                            "HIDE_PIN",
                                          ))
                                    : Text(_obscurePassword
                                        ? FlutterI18n.translate(
                                            context,
                                            "SHOW_PASSWORD",
                                          )
                                        : FlutterI18n.translate(
                                            context,
                                            "HIDE_PASSWORD",
                                          )),
                              ),
                            ]),
                          ),
                        );
                        buttons.add(DropdownMenuItem<String>(
                          value: "generate",
                          child: Row(children: <Widget>[
                            const Icon(
                              component.FontAwesome.cogs,
                              color: Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Text(FlutterI18n.translate(
                                  context, "GENERATE_PASSWORD")),
                            ),
                          ]),
                        ));
                      } else if (field.field == 'input' &&
                          field.type == 'totp_code') {
                        obscureText = true && _obscurePassword;
                        buttons.add(DropdownMenuItem<String>(
                          value: "show-hide",
                          child: Row(children: <Widget>[
                            Icon(
                              _obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: const Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Text(_obscurePassword
                                  ? FlutterI18n.translate(context, "SHOW")
                                  : FlutterI18n.translate(context, "HIDE")),
                            ),
                          ]),
                        ));
                      } else if (field.field == 'input' &&
                          field.type == 'checkbox') {
                      } else if (field.field == 'textarea' &&
                          field.type == 'password') {
                        keyboardType = TextInputType.multiline;
                        maxLines = _obscurePassword ? 1 : 10;

                        obscureText = true && _obscurePassword;
                        buttons.add(DropdownMenuItem<String>(
                          value: "show-hide",
                          child: Row(children: <Widget>[
                            Icon(
                              _obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: const Color(0xFF2dbb93),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Text(_obscurePassword
                                  ? FlutterI18n.translate(context, "SHOW")
                                  : FlutterI18n.translate(context, "HIDE")),
                            ),
                          ]),
                        ));
                      } else if (field.field == 'textarea') {
                        keyboardType = TextInputType.multiline;
                        maxLines = 10;
                      } else if (field.field == 'button' &&
                          field.type == 'button') {
                        keyboardType = TextInputType.multiline;
                      } else if (field.field == 'key_value_list') {
                      } else {
                        throw ("unknown field type combi");
                      }
                      buttons.add(DropdownMenuItem<String>(
                        value: "copy-to-clipboard",
                        child: Row(children: <Widget>[
                          const Icon(
                            component.FontAwesome.clipboard,
                            color: Color(0xFF2dbb93),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Text(field.field == 'input' &&
                                    field.type == 'password'
                                ? FlutterI18n.translate(
                                    context, "COPY_PASSWORD")
                                : FlutterI18n.translate(context, "COPY")),
                          ),
                        ]),
                      ));

                      List<Widget> children = [];
                      if (field.hidden) {
                        // The field is hidden lets not create a child for it
                      } else if (field.field == 'input' &&
                          field.type == 'totp_code') {
                        int? period;
                        if (_secret.data.containsKey('totp_period')) {
                          if (_secret.data['totp_period'] is String) {
                            period = int.parse(_secret.data['totp_period']);
                          } else {
                            period = _secret.data['totp_period'];
                          }
                        }
                        String? algorithm;
                        if (_secret.data.containsKey('totp_algorithm')) {
                          algorithm = _secret.data['totp_algorithm'];
                        }
                        int? digits;
                        if (_secret.data.containsKey('totp_digits')) {
                          if (_secret.data['totp_digits'] is String) {
                            digits = int.parse(_secret.data['totp_digits']);
                          } else {
                            digits = _secret.data['totp_digits'];
                          }
                        }

                        children.add(
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: component.TotpCode(
                              code: content,
                              digits: digits,
                              algorithm: algorithm,
                              period: period,
                            ),
                          ),
                        );
                      } else if (field.field == 'key_value_list') {
                        children.add(component.KeyValueList(
                          contentKV: contentKV,
                        ));
                      } else {
                        final textController = TextEditingController(
                          text: content,
                        );

                        controllers.add(textController);
                        children.add(
                          Stack(
                            alignment: Alignment.centerRight,
                            children: [
                              TextFormField(
                                obscureText: obscureText,
                                controller: textController,
                                inputFormatters: inputFormatters,
                                validator: (value) {
                                  if (field.required && value!.isEmpty) {
                                    return FlutterI18n.translate(
                                      context,
                                      field.errorMessageRequired!,
                                    );
                                  }
                                  return null;
                                },
                                keyboardType: keyboardType,
                                maxLines: maxLines,
                                onChanged: (text) {
                                  _secret.data[field.name] = text;
                                  if (field.field == 'input' &&
                                      ['website_password_url', 'bookmark_url']
                                          .contains(field.name) &&
                                      _bp!.urlfilterField != null) {
                                    if (text == '') {
                                      _secret.data[_bp!.urlfilterField] = '';
                                    } else {
                                      ParsedUrl parsedUrl =
                                          helper.parseUrl(text);
                                      _secret.data[_bp!.urlfilterField] =
                                          parsedUrl.authority;
                                    }
                                  }
                                },
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.fromLTRB(
                                        0, 10, 48.0, 10),
                                    labelText: FlutterI18n.translate(
                                        context, field.title!)),
                              ),
                              DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  icon: const Icon(
                                      component.FontAwesome.ellipsis_v),
                                  iconSize: 20,
                                  dropdownColor: Colors.white,
                                  onChanged: (String? newValue) async {
                                    if (newValue == 'copy-to-clipboard') {
                                      Clipboard.setData(
                                        ClipboardData(
                                          text: textController.text,
                                        ),
                                      );
                                      _clipboardCopyToast();
                                    }
                                    if (newValue == 'show-hide') {
                                      setState(() {
                                        _obscurePassword = !_obscurePassword;
                                      });
                                    }
                                    if (newValue == 'generate') {
                                      textController.text =
                                          await managerDatastorePassword
                                              .generate();
                                      _secret.data[field.name] =
                                          textController.text;
                                    }
                                  },
                                  items: buttons,
                                ),
                              )
                            ],
                          ),
                        );
                      }

                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: children,
                        ),
                      );
                    }
                  },
                ),
              ),
            ),
          ),
        ),
      );
    }
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            component.FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: const TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
        child: toast,
        toastDuration: const Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(top: 110, left: 0, right: 0, child: child);
        });
  }
}
