import 'package:json_annotation/json_annotation.dart';

import './read_metadata_secret.dart';

part 'read_metadata_share.g.dart';

@JsonSerializable()
class ReadMetadataShare {
  ReadMetadataShare({
    this.id,
    this.writeDate,
    this.shares,
    this.secrets,
  });

  final String? id;
  @JsonKey(name: 'write_date')
  final String? writeDate;
  List<ReadMetadataShare>? shares;
  List<ReadMetadataSecret>? secrets;

  factory ReadMetadataShare.fromJson(Map<String, dynamic> json) =>
      _$ReadMetadataShareFromJson(json);

  Map<String, dynamic> toJson() => _$ReadMetadataShareToJson(this);
}
