import 'package:json_annotation/json_annotation.dart';

part 'write_secret.g.dart';

@JsonSerializable()
class WriteSecret {
  WriteSecret({
    this.secretId,
  });

  @JsonKey(name: 'secret_id')
  final String? secretId;

  factory WriteSecret.fromJson(Map<String, dynamic> json) =>
      _$WriteSecretFromJson(json);

  Map<String, dynamic> toJson() => _$WriteSecretToJson(this);
}
