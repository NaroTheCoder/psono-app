import 'package:json_annotation/json_annotation.dart';

part 'delete_account.g.dart';

@JsonSerializable()
class DeleteAccount {
  DeleteAccount();

  factory DeleteAccount.fromJson(Map<String, dynamic> json) =>
      _$DeleteAccountFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteAccountToJson(this);
}
