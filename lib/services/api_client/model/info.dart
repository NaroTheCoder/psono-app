import 'dart:convert';
import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';

part 'info.g.dart';

@JsonSerializable()
class Info {
  Info([this.info, this.signature, this.verifyKey]) {
    if (this.info == null) {
      return;
    }

    Map<String, dynamic> decodedInfo = jsonDecode(this.info!);

    this.version = decodedInfo['version'] as String?;
    this.licenseType = decodedInfo['license_type'] as String?;
    this.licenseMaxUsers = decodedInfo['license_max_users'] as int?;
    this.licenseValidFrom = decodedInfo.containsKey('license_valid_from')
        ? new DateTime.fromMillisecondsSinceEpoch(
            (decodedInfo['license_valid_from'] as int) * 1000)
        : null;
    this.licenseValidTill = decodedInfo.containsKey('license_valid_till')
        ? new DateTime.fromMillisecondsSinceEpoch(
            (decodedInfo['license_valid_till'] as int) * 1000)
        : null;
    this.api = decodedInfo['api'] as int?;
    this.logAudit = decodedInfo['log_audit'] as bool?;
    this.publicKey = fromHex(decodedInfo['public_key'] as String?);
    this.authenticationMethods =
        new List<String>.from(decodedInfo['authentication_methods']);
    this.webClient = decodedInfo['web_client'] as String?;
    this.management = decodedInfo['management'] as bool?;
    this.multifactorEnabled = decodedInfo['multifactor_enabled'] as bool?;
    this.files = decodedInfo['files'] as bool?;

    this.complianceEnforce2fa =
        decodedInfo.containsKey('compliance_enforce_2fa')
            ? decodedInfo['compliance_enforce_2fa'] as bool?
            : false;
    this.complianceDisableExport =
        decodedInfo.containsKey('compliance_disable_export')
            ? decodedInfo['compliance_disable_export'] as bool?
            : false;
    this.complianceDisableDeleteAccount =
        decodedInfo.containsKey('compliance_disable_delete_account')
            ? decodedInfo['compliance_disable_delete_account'] as bool?
            : false;
    this.complianceDisableOfflineMode =
        decodedInfo.containsKey('compliance_disable_offline_mode')
            ? decodedInfo['compliance_disable_offline_mode'] as bool?
            : false;
    this.complianceMaxOfflineCacheTimeValid =
        decodedInfo.containsKey('compliance_max_offline_cache_time_valid')
            ? decodedInfo['compliance_max_offline_cache_time_valid'] as int?
            : 31536000;
    this.complianceDisableApiKeys =
        decodedInfo.containsKey('compliance_disable_api_keys')
            ? decodedInfo['compliance_disable_api_keys'] as bool?
            : false;
    this.complianceDisableEmergencyCodes =
        decodedInfo.containsKey('compliance_disable_emergency_codes')
            ? decodedInfo['compliance_disable_emergency_codes'] as bool?
            : false;
    this.complianceDisableRecoveryCodes =
        decodedInfo.containsKey('compliance_disable_recovery_codes')
            ? decodedInfo['compliance_disable_recovery_codes'] as bool?
            : false;
    this.complianceDisableFileRepositories =
        decodedInfo.containsKey('compliance_disable_file_repositories')
            ? decodedInfo['compliance_disable_file_repositories'] as bool?
            : false;
    this.complianceDisableLinkShares =
        decodedInfo.containsKey('compliance_disable_link_shares')
            ? decodedInfo['compliance_disable_link_shares'] as bool?
            : false;
    this.complianceMinMasterPasswordLength =
        decodedInfo.containsKey('compliance_min_master_password_length')
            ? decodedInfo['compliance_min_master_password_length'] as int?
            : 12;
    this.complianceMinMasterPasswordComplexity =
        decodedInfo.containsKey('compliance_min_master_password_complexity')
            ? decodedInfo['compliance_min_master_password_complexity'] as int?
            : 0;

    this.allowedSecondFactors =
        new List<String>.from(decodedInfo['allowed_second_factors']);
    this.allowUserSearchByEmail =
        decodedInfo['allow_user_search_by_email'] as bool?;
    this.allowUserSearchByUsernamePartial =
        decodedInfo['allow_user_search_by_username_partial'] as bool?;
    this.type = decodedInfo['type'] as String?;
  }

  final String? info;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List? signature;
  @JsonKey(name: 'verify_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? verifyKey;

  @JsonKey(ignore: true)
  String? version;
  @JsonKey(ignore: true)
  String? licenseType;
  @JsonKey(ignore: true)
  int? licenseMaxUsers;
  @JsonKey(ignore: true)
  DateTime? licenseValidFrom;
  @JsonKey(ignore: true)
  DateTime? licenseValidTill;
  @JsonKey(ignore: true)
  int? api;
  @JsonKey(ignore: true)
  bool? logAudit;
  @JsonKey(ignore: true)
  Uint8List? publicKey;
  @JsonKey(ignore: true)
  List<String>? authenticationMethods;
  @JsonKey(ignore: true)
  String? webClient;
  @JsonKey(ignore: true)
  bool? management;
  @JsonKey(ignore: true)
  bool? multifactorEnabled;
  @JsonKey(ignore: true)
  bool? files;

  @JsonKey(ignore: true)
  bool? complianceEnforce2fa;
  @JsonKey(ignore: true)
  bool? complianceDisableExport;
  @JsonKey(ignore: true)
  bool? complianceDisableDeleteAccount;
  @JsonKey(ignore: true)
  bool? complianceDisableOfflineMode;
  @JsonKey(ignore: true)
  int? complianceMaxOfflineCacheTimeValid;
  @JsonKey(ignore: true)
  bool? complianceDisableApiKeys;
  @JsonKey(ignore: true)
  bool? complianceDisableEmergencyCodes;
  @JsonKey(ignore: true)
  bool? complianceDisableRecoveryCodes;
  @JsonKey(ignore: true)
  bool? complianceDisableFileRepositories;
  @JsonKey(ignore: true)
  bool? complianceDisableLinkShares;
  @JsonKey(ignore: true)
  int? complianceMinMasterPasswordLength;
  @JsonKey(ignore: true)
  int? complianceMinMasterPasswordComplexity;

  @JsonKey(ignore: true)
  List<String>? allowedSecondFactors;
  @JsonKey(ignore: true)
  bool? allowUserSearchByEmail;
  @JsonKey(ignore: true)
  bool? allowUserSearchByUsernamePartial;
  @JsonKey(ignore: true)
  String? type;

  factory Info.fromJson(Map<String, dynamic> json) => _$InfoFromJson(json);

  Map<String, dynamic> toJson() => _$InfoToJson(this);
}
