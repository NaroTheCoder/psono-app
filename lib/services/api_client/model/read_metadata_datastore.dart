import 'package:json_annotation/json_annotation.dart';

import './read_metadata_secret.dart';
import './read_metadata_share.dart';

part 'read_metadata_datastore.g.dart';

@JsonSerializable()
class ReadMetadataDatastore {
  ReadMetadataDatastore({
    this.writeDate,
    this.shares,
    this.secrets,
  });

  @JsonKey(name: 'write_date')
  final String? writeDate;
  List<ReadMetadataShare>? shares;
  List<ReadMetadataSecret>? secrets;

  factory ReadMetadataDatastore.fromJson(Map<String, dynamic> json) =>
      _$ReadMetadataDatastoreFromJson(json);

  Map<String, dynamic> toJson() => _$ReadMetadataDatastoreToJson(this);
}
