import 'package:json_annotation/json_annotation.dart';

part 'read_datastore_list.g.dart';

@JsonSerializable()
class ReadDatastoreList {
  ReadDatastoreList({
    this.datastores,
  });
  List<ReadDatastoreListEntry>? datastores;

  factory ReadDatastoreList.fromJson(Map<String, dynamic> json) =>
      _$ReadDatastoreListFromJson(json);

  Map<String, dynamic> toJson() => _$ReadDatastoreListToJson(this);
}

@JsonSerializable()
class ReadDatastoreListEntry {
  ReadDatastoreListEntry({
    this.id,
    this.type,
    this.description,
    this.isDefault,
  });

  final String? id;
  final String? type;
  String? description;
  @JsonKey(name: 'is_default')
  bool? isDefault;

  factory ReadDatastoreListEntry.fromJson(Map<String, dynamic> json) =>
      _$ReadDatastoreListEntryFromJson(json);

  Map<String, dynamic> toJson() => _$ReadDatastoreListEntryToJson(this);
}
