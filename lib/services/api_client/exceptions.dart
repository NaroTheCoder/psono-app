import 'dart:convert';

class BadRequestException implements Exception {
  final int statusCode = 400;
  String? message;
  Map? jsonMessage;

  BadRequestException(String? message) {
    this.message = message;
    if (message != null && message != '') {
      try {
        this.jsonMessage = jsonDecode(message);
      } catch (e) {
        // pass
      }
    }
  }

  String toString() => message!;

  String getFirst() {
    if (this.jsonMessage != null) {
      return this.jsonMessage!.values.toList().first[0];
    }
    if (message == null) {
      return '';
    } else {
      return message!;
    }
  }

  String getByKey(String key) {
    if (this.jsonMessage != null && this.jsonMessage!.containsKey(key)) {
      if (this.jsonMessage![key] is List) {
        if (this.jsonMessage![key].length > 0) {
          return this.jsonMessage![key][0];
        } else {
          return '';
        }
      }
      return this.jsonMessage![key];
    }
    return '';
  }
}

class UnauthorizedException implements Exception {
  final int statusCode = 401;
  final String? message;

  UnauthorizedException(this.message);

  String toString() => message!;
}

class ForbiddenException implements Exception {
  final int statusCode = 403;
  final String? message;

  ForbiddenException(this.message);

  String toString() => message!;
}

class InternalServerErrorException implements Exception {
  final int statusCode = 500;
  final String? message;

  InternalServerErrorException(this.message);

  String toString() => message!;
}

class ServiceUnavailableException implements Exception {
  final int statusCode = 503;
  final String? message;

  ServiceUnavailableException(this.message);

  String toString() => message!;
}

class UnknownException implements Exception {
  final int statusCode;
  final String? message;

  UnknownException(this.message, this.statusCode);

  String toString() => message!;
}
