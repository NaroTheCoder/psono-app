import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'encrypted_data.g.dart';

/// Represents a encrypted data with the cipher text and the nonce
@JsonSerializable()
class EncryptedData {
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List? text;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List? nonce;

  const EncryptedData(this.text, this.nonce);
}
